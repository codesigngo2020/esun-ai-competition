# Esun AI Competition

本專案為參加[「玉山人工智慧挑戰賽 2021 夏季賽」](https://tbrain.trendmicro.com.tw/Competitions/Details/14)的程式碼。

## 競賽內容概要

**分類項目**

- 800 個繁體字的圖像分類
- 若在 800 個類別外的圖片，判定為 isnull。

**訓練資料**

- 官方提供[訓練圖片集](官方文件/train)以及各自 label，但不保證 label 正確，須自行資料清洗。

## 專案導覽

**官方文件**

一切官方提供的競賽所需文件以及訓練資料皆存放在[官方文件](官方文件)中。

**訓練過程**

專案依序進行[資料清洗](資料清洗)、[資料擴增](資料擴增)、[模型訓練](模型訓練)

1. 資料清洗：清洗官方提供的訓練資料。
2. 資料擴增：增加更多外部資料，包含簡體字。
3. 模型訓練：訓練出最終模型，以及評估。

## Step1：資料清洗

**依續步驟**

1. 在 [prepare_dataset.ipynb](資料清洗/1\)_prepare_dataset.ipynb) 中透過 python cv2 將官方彩色圖片轉為黑白，同時移除雜訊。
2. 引入外部資料集 [Traditional-Chinese-Handwriting-Dataset](https://github.com/AI-FREE-Team/Traditional-Chinese-Handwriting-Dataset)。
3. 訓練 10 個 Model，自我排除錯誤標示的圖片。<br>將資料集拆為 10 份，其中 4 份作為訓練資料（灰色），3 份作為驗證資料（橘色），3 份作為測試資料（藍色），其概念為：透過訓練和驗證資料來判斷測試資料是否在分佈內（和訓練和驗證資料相近）。依此執行 10 次，每完成一次移動一格，因此最後每一張圖片會被票選 3 次（票選是否和其他圖片相近）。

![](assets/img_1.png)

**結果**

每張圖片最後會被票選 3 次，以下是被票選出有問題的次數、圖片數以及佔比。（在後續的訓練中會僅留下`相異次數`為 0 的圖片）

票選相異次數 | 圖片數     | 佔比
-----------|:---------:|------------
0          | 57064     | 0.829370
1          | 5073      | 0.073731
2          | 3302      | 0.047991
3          | 3365      | 0.048907

## Step2：資料擴增

由於會有需要判斷簡體字，因此載入 [CASIA Handwriting Dataset](https://www.kaggle.com/pascalbliem/handwritten-chinese-character-hanzi-datasets)。

## Step3：模型訓練

以 EfficientNet B3 作為 Base model。在處理 OOD（Out Of Distribution）上嘗試以下 3 種方式：

- [Learning Confidence for Out-of-Distribution Detection in Neural Network](https://arxiv.org/pdf/1802.04865.pdf)
- [Feature Space Singularity for Out-of-Distribution Detection](https://arxiv.org/pdf/2011.14654.pdf)
- [Unsupervised Out-of-Distribution Detection by Maximum Classifier Discrepancy](https://openaccess.thecvf.com/content_ICCV_2019/papers/Yu_Unsupervised_Out-of-Distribution_Detection_by_Maximum_Classifier_Discrepancy_ICCV_2019_paper.pdf)

最終採用最後一種（Unsupervised Out-of-Distribution Detection by Maximum Classifier Discrepancy）並以 Traditional-Chinese-Handwriting-Dataset 800 字以外的圖片最為 OOD 圖片進行學習，詳細訓練過程見[連結](模型訓練/ood_by_mcd_b3)。

# 結果說明

比賽最終結果，整體的 Macro F1-score 落在 0.85 ~ 0.87 之間，以下列出模型幾
項問題：

1. 圖片過度簡化<br>在圖片進入模型前，都會透過 cv2 轉黑白，僅留下較明顯的線條輪廓。
    - 問題：這導致圖片的特徵減少，尤其在線條輪廓不夠明顯時，會完全抓不到任何訊號。
    - 改善方式：透過訓練模型，以 CNN 學習的方式來抓出文字輪廓，降低背景訊號。
2. 淘汰過多樣本<br>在資料清洗階段採用樣本間相互投票的方式來淘汰差異過大的樣本，視為極可能是錯誤標記的樣本。
    - 問題：此方法導致樣本間的多樣化減少，因為留下來的樣本都是相近的圖片。
    - 改善方式：可以將最終模型再對遭移除的資料進行評估，重新檢視並把部分原先淘汰的圖片撈回來。
3. OOD 判斷不易<br>採用「Unsupervised Out-of-Distribution Detection by Maximum Classifier Discrepancy」的方式進行 OOD 訓練，訓練時所使用的 Threshold 不符合比賽狀況。
    - 問題：導入的 OOD 圖片雖然有助於決策邊界的精確度上升，但整體模型信心度下降（max softmax probability 下降），且訓練的 OOD 也不符合比賽時會遇到的 OOD 圖片。
    - 改善方式：
        1. 採用更貼近比賽時會遇到的 OOD 圖片進行 Unsupervised 訓練。
        2. 訓練更廣泛的文字類別，訓練常用 4000 字，非 800 字的及判斷為 OOD。
        3. 尋找更好的 OOD 判斷方式。

# 收穫

- OOD 探討<br>參考許多 OOD 判斷方式，個人認為「Unsupervised Out-of-Distribution Detection by Maximum Classifier Discrepancy」在了解 OOD 大致情況的題目下，提供 OOD 樣本有助於讓模型鑑別出 In-Distribution 和 Out-id-Distribution，但在提供的 OOD 樣本非真實情況時，模型容易錯亂。
- API Server<br>本次採用在 GCP 上開一台小台的 vm，當未掛載 GPU 下，每 request 約需 600～800 ms 回應時間；在掛載一顆 K80 GPU 下，可以提升至 100～120 ms。

# 延伸

- 下次圖像辨識可以嘗試 Swin Transformer (Swin-tiny)，從 model-based 方向優化。
