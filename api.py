from argparse import ArgumentParser
import base64
import datetime
import hashlib

import cv2
from flask import Flask
from flask import request
from flask import jsonify
import numpy as np

# my packages
import cv2
import gc
import os
import glob
import json
import math
import time
import random
import numpy as np
import pandas as pd
import seaborn as sns
import tensorflow as tf
from tensorflow.keras import backend as K
from shutil import copyfile
from tqdm import tqdm, trange
from PIL import Image
import matplotlib
import matplotlib.patheffects as PathEffects
import matplotlib.pyplot as plt
from matplotlib import pyplot as plt
from functools import partial, reduce
from IPython.display import clear_output
from sklearn.utils import shuffle, resample
from sklearn.linear_model import LogisticRegressionCV
from sklearn.model_selection import KFold, train_test_split


app = Flask(__name__)

####### PUT YOUR INFORMATION HERE #######
CAPTAIN_EMAIL = 'codesigngo2020@gmail.com'          #
SALT = '1205'                        #
#########################################


def generate_server_uuid(input_string):
    """ Create your own server_uuid.

    @param:
        input_string (str): information to be encoded as server_uuid
    @returns:
        server_uuid (str): your unique server_uuid
    """
    s = hashlib.sha256()
    data = (input_string + SALT).encode("utf-8")
    s.update(data)
    server_uuid = s.hexdigest()
    return server_uuid


def base64_to_binary_for_cv2(image_64_encoded):
    """ Convert base64 to numpy.ndarray for cv2.

    @param:
        image_64_encode(str): image that encoded in base64 string format.
    @returns:
        image(numpy.ndarray): an image.
    """
    img_base64_binary = image_64_encoded.encode("utf-8")
    img_binary = base64.b64decode(img_base64_binary)
    image = cv2.imdecode(np.frombuffer(img_binary, np.uint8), cv2.IMREAD_COLOR)
    return image


def predict(image, esun_timestamp):
    """ Predict your model result.

    @param:
        image (numpy.ndarray): an image.
    @returns:
        prediction (str): a word.
    """

    ####### PUT YOUR MODEL INFERENCING CODE HERE #######
    # Image.fromarray(image).save('test.jpg')

    Image.fromarray(image).save('requests/%s.jpg' % str(esun_timestamp))

    image = rgb_to_binary(image)

    x = image[np.newaxis,:]
    x = x/255.

    pred_1 = model_1.predict(x)
    pred_2 = model_2.predict(x)

    loss = unsup_loss(pred_1, pred_2)
    loss = loss[0]

    print(pred_1.max())
    print(pred_2.max())
    print(loss)

    if (loss < 0.74) | (pred_1.max() < 0.53):
        prediction = 'isnull'
    else:
        pred = np.argmax(pred_1[:,:-1], 1)
        pred = pred[0]

        # prediction = '陳'
        prediction = index_label[pred]

    Image.fromarray(image).save('requests/%s_%s.jpg' % (str(esun_timestamp), prediction))

    ####################################################
    if _check_datatype_to_string(prediction):
        return prediction


def _check_datatype_to_string(prediction):
    """ Check if your prediction is in str type or not.
        If not, then raise error.

    @param:
        prediction: your prediction
    @returns:
        True or raise TypeError.
    """
    if isinstance(prediction, str):
        return True
    raise TypeError('Prediction is not in string type.')


@app.route('/inference', methods=['POST'])
def inference():
    """ API that return your model predictions when E.SUN calls this API. """
    data = request.get_json(force=True)

    # 自行取用，可紀錄玉山呼叫的 timestamp
    esun_timestamp = data['esun_timestamp']

    # 取 image(base64 encoded) 並轉成 cv2 可用格式
    image_64_encoded = data['image']
    image = base64_to_binary_for_cv2(image_64_encoded)

    t = datetime.datetime.now()
    ts = str(int(t.utcnow().timestamp()))
    server_uuid = generate_server_uuid(CAPTAIN_EMAIL + ts)

    try:
        answer = predict(image, esun_timestamp)
    except TypeError as type_error:
        # You can write some log...
        raise type_error
    except Exception as e:
        # You can write some log...
        raise e
    server_timestamp = time.time()

    return jsonify({'esun_uuid': data['esun_uuid'],
                    'server_uuid': server_uuid,
                    'answer': answer,
                    'server_timestamp': server_timestamp})


def rgb_to_binary(img):
    gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

    # binarize the image
    ret, bw = cv2.threshold(gray, 128, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)

    # find connected components
    connectivity = 8
    nb_components, output, stats, centroids = cv2.connectedComponentsWithStats(bw, connectivity, cv2.CV_32S)
    sizes = stats[1:, -1]
    nb_components = nb_components - 1

    # 白底黑字 image
    new_img = np.full((img.shape), 255, np.uint8)
    img_area = new_img.shape[0] * new_img.shape[1]

    for i in range(0, nb_components):
        new_img[output == i + 1] = (0,0,0)

    side_length = max(img.shape[:-1])
    padded_img = np.full((side_length, side_length, 3), 255, np.uint8)

    h, w = img.shape[:-1]
    y, x = int((side_length-h)/2), int((side_length-w)/2)

    padded_img[y:y+h, x:x+w, :] = new_img

    image = cv2.resize(padded_img, (128, 128), interpolation=cv2.INTER_AREA)
    # Image.fromarray(image).save('requests/%s.jpg' % str(esun_timestamp))
    # Image.fromarray(padded_img).resize((50,50)).save('rgb_to_binary.jpg')
    return image

if __name__ == "__main__":
    arg_parser = ArgumentParser(
        usage='Usage: python ' + __file__ + ' [--port <port>] [--help]'
    )
    arg_parser.add_argument('--host', default='127.0.0.1', help='host')
    arg_parser.add_argument('-p', '--port', default=8080, help='port')
    arg_parser.add_argument('-d', '--debug', default=True, help='debug')
    options = arg_parser.parse_args()

    # my code

    # ========================================

    try:
        tpu = tf.distribute.cluster_resolver.TPUClusterResolver()
        print('Device:', tpu.master())
        tf.config.experimental_connect_to_cluster(tpu)
        tf.tpu.experimental.initialize_tpu_system(tpu)
        strategy = tf.distribute.experimental.TPUStrategy(tpu)
    except:
        strategy = tf.distribute.get_strategy()
    print('Number of replicas:', strategy.num_replicas_in_sync)

    # ========================================

    class FixedDropout(tf.keras.layers.Dropout):
        def _get_noise_shape(self, inputs):
            if self.noise_shape is None:
                return self.noise_shape
            symbolic_shape = K.shape(inputs)
            noise_shape = [symbolic_shape[axis] if shape is None else shape
            for axis, shape in enumerate(self.noise_shape)]
            return tuple(noise_shape)

    # ========================================

    def get_supervised_model():
        with strategy.scope():
            supervised_model = tf.keras.models.load_model('模型訓練/models/step_1_best_model.h5',
                                            custom_objects={
                                                'swish': tf.compat.v2.nn.swish,
                                                'FixedDropout':FixedDropout
                                            })
        return supervised_model
    supervised_model = get_supervised_model()
    supervised_model.trainable = False

    # ========================================

    def create_model():
        cloned_supervised_models = []
        for i in range(2):
            cloned_model = get_supervised_model()
            # cloned_model.trainable = False
            for layer in cloned_model.layers:
                layer._name = layer.name + '_%d'%i
            cloned_supervised_models.append(cloned_model)

        ind_input = tf.keras.Input(shape=(128,128,3), batch_size=None)
        ood_input = tf.keras.Input(shape=(128,128,3), batch_size=None)

        x_1 = supervised_model.layers[0](ind_input)
        for layer in cloned_supervised_models[0].layers[1:]:
            x_1 = layer(x_1)

        x_2 = supervised_model.layers[0](ind_input)
        for layer in cloned_supervised_models[1].layers[1:]:
            x_2 = layer(x_2)

        x_3 = supervised_model.layers[0](ood_input)
        for layer in cloned_supervised_models[1].layers[1:]:
            x_3 = layer(x_3)

        outputs = tf.keras.layers.Concatenate(axis=1)([x_1, x_2, x_3])

        model = tf.keras.Model(inputs=[ind_input, ood_input], outputs=outputs)

        return model

    # ========================================

    with strategy.scope():
        ood_model_1 = create_model()
        ood_model_1.load_weights('模型訓練/models/step_3_best_model_1.h5')

        ood_model_2 = create_model()
        ood_model_2.load_weights('模型訓練/models/step_3_best_model_2.h5')

    # ========================================

    with strategy.scope():
        inputs_1 = ood_model_1.layers[0].input

        x_1 = ood_model_1.get_layer('efficientnetb3')(inputs_1)
        for layer_name in ('dropout_2_0', 'dense_2_0'):
            x_1 = ood_model_1.get_layer(layer_name)(x_1)

        model_1 = tf.keras.Model(inputs=inputs_1, outputs=x_1)

        inputs_2 = ood_model_2.layers[1].input
        x_2 = ood_model_2.get_layer('efficientnetb3')(inputs_2)
        for layer_name in ('dropout_2_1', 'dense_2_1'):
            x_2 = ood_model_2.get_layer(layer_name)(x_2)

        model_2 = tf.keras.Model(inputs=inputs_2, outputs=x_2)

    # ========================================

    def unsup_loss(input_1, input_2):
        entropy_1 = - input_1 * (input_1 - K.log(K.sum(K.exp(input_1), axis=1, keepdims=True)))
        entropy_2 = - input_2 * (input_2 - K.log(K.sum(K.exp(input_2), axis=1, keepdims=True)))

        entropy_1 = K.sum(entropy_1, axis=1)
        entropy_2 = K.sum(entropy_2, axis=1)

        unsup_loss = 1 - K.abs(entropy_1 - entropy_2)
        unsup_loss = unsup_loss.numpy()
        return unsup_loss

    # ========================================

    file = open('官方文件/training data dic.txt', 'r')
    word_index = {}

    for i, line in enumerate(file.readlines()):
        word = line.strip()
        if len(word) != 1:
            print('Error word: %s' % word)
        word_index[word] = i

    file.close()

    # ========================================

    label_index = word_index
    index_label = {label_index[word] : word for word in label_index}

    # ========================================

    if os.path.exists('requests') == False:
        os.mkdir('requests')

    # ========================================

    app.run(debug=options.debug, host=options.host, port=options.port)
