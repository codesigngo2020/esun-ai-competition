# API Server 設定

此資料夾作為最終 API Server 建立用。

## 指令

### SSH to API Server

```
# SSH 到 API Server
$ ssh 35.201.245.247

# 啟動 API Server
$ CUDA_VISIBLE_DEVICES=1 python api.py --host=0.0.0.0 --p=8081

```

### Local Terminal

```
# 上傳 submit 資料夾到 API Server 指定路徑
$ scp -r ./submit/ 35.201.245.247:./Esun-AI-Competition/

# 下載儲存 request 的圖片到 Local
$ scp -r 35.201.245.247:./Esun-AI-Competition/submit/requests /Users/scott.tsai/Downloads
```
