# Unsupervised Out-of-Distribution Detection by Maximum Classifier Discrepancy

參考 [Unsupervised Out-of-Distribution Detection by Maximum Classifier Discrepancy](https://openaccess.thecvf.com/content_ICCV_2019/papers/Yu_Unsupervised_Out-of-Distribution_Detection_by_Maximum_Classifier_Discrepancy_ICCV_2019_paper.pdf) 論文，透過同時提供 ind (in distribution) 和 ood (out of distribution) 圖片，進行 Unsupervised Learning。

## 依序步驟

1. 提供 ind 圖片，進行 supervised Learning，此時 model 架構為 one input 以及 one output。
2. 將前步的 model 改為 two inputs（一個 input ind images；另一個 input ood images）以及兩個分類器，並自定義 loss function，同時考慮兩個分類器的 sparse categorical crossentropy 以及 ind 和 ood 分佈上的差異。
3. 評估一個一個適當的 threshold 來判斷是否為 ood。

![](assets/img_13.png)

## 結果說明

### Decision Boundary 內縮

利用兩個分類器去進行拮抗，主要的目的是想達成「決策邊界」的精準化。

![](assets/img_14.png)

由下方 Softmax 比較圖可以看出經過 Unsupervised Learning 後，ood 的 softmax「最大機率」都有左移。推斷此為 decision boundary 內縮的結果。

**Supervised**

Softmax 最大機率佔比      | Softmax 最大機率佔比累計
----------------------- | -----------------------
![](assets/img_1.png)   | ![](assets/img_2.png)

**Unsupervised Classifier 1**

Softmax 最大機率佔比      | Softmax 最大機率佔比累計
----------------------- | -----------------------
![](assets/img_6.png)   | ![](assets/img_7.png)

**Unsupervised Classifier 2**

Softmax 最大機率佔比      | Softmax 最大機率佔比累計
----------------------- | -----------------------
![](assets/img_8.png)   | ![](assets/img_9.png)

### Threshold

最終在犧牲 0.029459 準確度的容忍下，換取 0.499604 的 ood 判定準確率。

- unsup_loss_threshold：0.74
- max_prob_threshold：0.53

下圖為犧牲 0.03 準確度以下，不同 unsup_loss_threshold 和 max_prob_threshold 組合所能判斷出 ood 的準確率。取用的組合會盡可能在右下角的前緣處。

![](assets/img_12.png)

## 結論

當 unsup_loss < 0.74 或 max_prob < 0.53 時，及判定為 isnull；反之，使用 Unsupervised Classifier 1 的分類結果（Classifier 1 比 Classifier 2 的準確率更高一點，因為在 Unsupervised Learning 時，ood 圖片是經由 Classifier 2）學習。
